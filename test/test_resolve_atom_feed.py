"""Test the atom feed features of atom_signal_mirror."""
import os.path
from atom_signal_mirror.main import resolve_atom_feed
import unittest


class TestProperDateSelection(unittest.TestCase):

    def test_proper_date_selection(self):
        """Test to resolve the test data atom feed with a fixed last_check date.

        The test checks the number of elements and their correct order
        """
        with open('./test/temp/last_check.txt', 'w') as temp_file:
            temp_file.write('2021-12-09T08:00:00.000000+01:00')
        messages = resolve_atom_feed(f"file:///{os.path.abspath('./test/test_data/atom.xml')}", './test/temp')
        self.assertEqual(3, len(messages), 'Resolving needs to create 3 messages')
        self.assertTrue('Bitte Story checken und Beitrag der Berliner Stadtmission teilen' in messages[0], (
            "First message should be about Özdemir's new Marihuana Christmas Tree! Ringing a bell? "
            "What the hell did you do while coding?! You can tell me now..."
        ))
        self.assertTrue("Lebenszeichen vom Daddy" in messages[1], "Second message should contain life signs of Markus Söder. Sorry about that.")
        self.assertTrue("schön die Leber geboostert - dann Vereidigung" in messages[2], (
            "Third messa is about the swearin-in of Prof. Lauterbach, minister of health, from 2021. Listen in yourself, what should I say..."
        ))


if __name__ == '__main__':
    unittest.main()
