"""Test to call the app with arguments but mocking the rest."""
from os import path, remove
import atom_signal_mirror.main
import unittest
from unittest.mock import MagicMock, patch


class TestCallWithArgs(unittest.TestCase):
    """Test the main entry point of the application with different parameters.

    Goal of the test is to ensure the application workflow is initalized correctly.s
    """

    @patch("atom_signal_mirror.main.resolve_atom_feed")
    def test_no_atom_feed_data(self, resolve_atom_feed):
        """Test that in the case where now Atom Feed data is available neither QR code nor receive messages is called."""
        resolve_atom_feed.return_value = None
        atom_signal_mirror.main.create_qr_code = MagicMock()
        atom_signal_mirror.main.receive_messages = MagicMock()
        atom_feed_url = f"file:///{path.abspath('./test/test_data/atom.xml')}"
        atom_signal_mirror.main.atom_signal_mirror("Test CLI", "./test/test_data/signal-cli-binaries",
                                                   "./test/test_data/signal-cli", "./test/test_data/temp", atom_feed_url, False)
        atom_signal_mirror.main.create_qr_code.assert_not_called()
        atom_signal_mirror.main.receive_messages.assert_not_called()

    def test_valid_atom_feed_data(self):
        """Test that in the case where Atom Feed data is available the function to create a QR code is called."""
        with open('./test/test_data/temp/last_check.txt', 'w') as temp_file:
            temp_file.write('2021-12-09T08:00:00.000000+01:00')
        atom_signal_mirror.main.create_qr_code = MagicMock()
        atom_signal_mirror.main.receive_messages = MagicMock()
        atom_feed_url = f"file:///{path.abspath('./test/test_data/atom.xml')}"
        atom_signal_mirror.main.atom_signal_mirror("Test CLI", "./test/test_data/signal-cli-binaries",
                                                   "./test/test_data/signal-cli", "./test/test_data/temp", atom_feed_url, False)
        atom_signal_mirror.main.create_qr_code.assert_called()

    def test_after_signal_linkactive(self):
        """Test that in the case where a Signal account has been linked receive messages is called."""
        with open("./test/test_data/temp/last_check.txt", 'w') as temp_file:
            temp_file.write('2021-12-09T08:00:00.000000+01:00')
        with open("./test/test_data/signal-cli/fake_link_established.txt", "w") as temp_file:
            temp_file.write('42')
        atom_signal_mirror.main.create_qr_code = MagicMock()
        atom_signal_mirror.main.receive_messages = MagicMock()
        atom_signal_mirror.main.resolve_group_id_from_name = MagicMock()
        atom_signal_mirror.main.send_message = MagicMock()
        atom_feed_url = f"file:///{path.abspath('./test/test_data/atom.xml')}"
        atom_signal_mirror.main.atom_signal_mirror("Test CLI", "./test/test_data/signal-cli-binaries",
                                                   "./test/test_data/signal-cli", "./test/test_data/temp", atom_feed_url, False)
        atom_signal_mirror.main.create_qr_code.assert_not_called()
        atom_signal_mirror.main.receive_messages.assert_called()
        atom_signal_mirror.main.send_message.assert_called()
        remove("./test/test_data/signal-cli/fake_link_established.txt")


if __name__ == '__main__':
    unittest.main()
