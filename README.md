# atom-signal-mirror

![Code Coverage](coverage.svg)

Calling the script mirrors the last entry in the given atom xml feed to a signal group. The first run will initiate linking the given phone number to an existing Signal account. This is the only mode currently supported.

## Usage
```
python atom-signal-mirror.py [-h] [--dry-run DRY_RUN] [--signal-cli-cfg SIGNAL_CLI_CFG] [--signal-cli-path SIGNAL_CLI_PATH] atom_feed_url phone_number target_group

Send a source atom feed to a signal target group. Right now only works with an already registered phone number at Signal and links as an additional device to that account (like signal-
desktop)

positional arguments:
  atom_feed_url         Source atom feed URL
  phone_number          Sender phone number to send messages from
  target_group          Target group to send messages

optional arguments:
  -h, --help            show this help message and exit
  --dry-run DRY_RUN     If set to True the script will run with the sandbox environment of Signal
  --signal-cli-cfg SIGNAL_CLI_CFG
                        Path to the signal-cli config directory
  --signal-cli-path SIGNAL_CLI_PATH
                        Path to the signal-cli executable


# Example
python atom-signal-mirror.py https://insta.trom.tf/u/hauptstadt.memes/atom.xml +491234567890 "Test CLI"
```
## Run Tests
```bash
# Run tests
coverage run -m unittest discover

# Create coverage report
coverage report --omit "test/*","**/__init__.py"
coverage xml # XML can be used by VSCode Coverage Gutters

# Create coverage badge
coverage-badge -o coverage.svg
```
## Docker

```bash
docker build . -t atom-signal-mirror   
docker run -v $(pwd)/signal-cli:/app/atom-signal-mirror/signal-cli -v $(pwd)/temp:/app/atom-signal-mirror/temp --rm atom-signal-mirror https://insta.trom.tf/u/hauptstadt.memes/atom.xml +491234567890 "My group name"

```
## Installation

* Python 3.9.x
* Java 11
* signal-cli installed at ./signal-cli-binaries

### Arch Linux / Manjaro
```bash
sudo pacman -S jre11-openjdk 
wget https://github.com/AsamK/signal-cli/releases/download/v0.9.2/signal-cli-0.9.2.tar.gz
tar xfvz signal-cli-0.9.2.tar.gz
rm signal-cli-0.9.2.tar.gz
mv signal-cli-0.9.2 signal-cli-binaries
```

### Debian 10
Use the provided Dockerfile as libsignal is incompatible (libc6 < 2.29)

### Ubuntu 20.04
```bash
apt-get install wget openjdk-11-jre git python3 python3-pip -y
wget https://github.com/AsamK/signal-cli/releases/download/v0.9.2/signal-cli-0.9.2.tar.gz
tar xfvz signal-cli-0.9.2.tar.gz
rm signal-cli-0.9.2.tar.gz
mv signal-cli-0.9.2 signal-cli-binaries
```
# Acknowledgments
signal-cli - https://github.com/AsamK/signal-cli