"""Mirror script to post data from an atom XML feed to a Signal group.

The script will, when started for the first time, link to an existing Signal phone number.
If already linked, the script will post the last post of the atom feed to the provided target group name.

  Typical usage example:

  python atom-signal-mirror.py https://source-atom-feed "My target group name"
"""

# OPTIONALS
# TODO Render qrcode smaller (no scale parameter available for terminal function. Smallest possible version in create() was 9 during tests, still too large)

from typing import List
import argparse
import atoma
import os
import subprocess
import urllib.request
from urllib.parse import unquote
import pyqrcode
import re
from io import StringIO
from html.parser import HTMLParser
from datetime import timedelta, datetime

qrcode_file_name = "qrcode.png"


def atom_signal_mirror(target_group, signal_cli_path, signal_cli_cfg, temp_dir, atom_feed_url, dry_run):
    """Check the current state and delegates to the different operations.

    Args:
        target_group (str): Name of the group to send messages to
        signal_cli_path (str): Path where the signal-cli executable can be found in the system
        signal_cli_cfg (str): Path to the signal-cli config directory
        dry_run (bool): If true, the sandbox environment of Signal will be used
    """
    print(f"Started atom-signal-mirror for feed {atom_feed_url} to target group {target_group}")

    if not os.path.isdir(temp_dir):
        os.mkdir(temp_dir)

    if not os.path.isdir(signal_cli_cfg):
        os.mkdir(signal_cli_cfg)

    messages = resolve_atom_feed(atom_feed_url, temp_dir)
    print(messages)
    if messages:
        signal_cli_args = [signal_cli_path,
                           "--config", signal_cli_cfg]

        if dry_run:
            print("Started with --dry-run. Will only contact Signal's sandbox environment")
            signal_cli_args.append("--service-environment")
            signal_cli_args.append("sandbox")

        if not os.listdir(signal_cli_cfg):
            create_qr_code(signal_cli_args.copy())
        else:
            receive_messages(signal_cli_args.copy())
            group_id = resolve_group_id_from_name(signal_cli_args.copy(), target_group)
            if group_id:
                for message in messages:
                    send_message(signal_cli_args.copy(), group_id, message, temp_dir)


def send_message(signal_cli_args, group_id, message, temp_dir):
    """Send the given message to the group_id.

    Will update the message by removing html tags and download and attach a picture, if it exists

    Args:
        signal_cli_args (str[]): Signal CLI command without any operation set
        group_id (str): ID of the group to send the message to
        message (str): Message to be sent (raw HTML)
    """
    edited_message = strip_tags(message)
    edited_message = edited_message.replace(".\n", "")
    print(f"Sending message to Signal: {edited_message}")
    signal_cli_args.extend(["send", "--group", group_id, "-m", f'"[BOT]: {edited_message}"'])
    media_tag = re.search("(?P<url>(img|video) src=\"https?://[^\s]+)", message)
    suffix = ".png"
    if ".mp4" in message:
        suffix = ".mp4"

    if media_tag:
        media_url_tag = media_tag.group("url")
        if media_url_tag:
            media_url = media_url_tag.split('"')[1]
            urllib.request.urlretrieve(media_url, f"{temp_dir}/resource{suffix}")
            signal_cli_args.extend(["-a", f"{temp_dir}/resource{suffix}"])
        with subprocess.Popen(signal_cli_args, stdout=subprocess.PIPE) as popen:
            popen.stdout.read()
    else:
        print("Unsupported attachment type. Ignoring")
        # TODO Support more mimetypes


def resolve_atom_feed(atom_feed_url: str, temp_dir: str) -> List[str]:
    """Resolve the given atom feed url and return a list of entries that changed since the last check.

    If running for the first time the last check date defaults to now() - 8 hours.

    Args:
        atom_feed_url (str): URL of the atom feed
        temp_dir (str): Path to the atom feed

    Returns:
        List[str]: Entries since the last check
    """
    entries = []

    # Default last check to 8 hours (if running for the first time)
    last_check = datetime.now().astimezone() - timedelta(hours=8)

    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)

    if os.path.isfile(f"{temp_dir}/last_check.txt"):
        with open(f"{temp_dir}/last_check.txt", "r") as last_check_file:
            last_check_str = last_check_file.readline()
            last_check = datetime.fromisoformat(last_check_str)

    urllib.request.urlretrieve(atom_feed_url, f"{temp_dir}/atom.xml")
    feed = atoma.parse_atom_file(f"{temp_dir}/atom.xml")
    for feed_entry in feed.entries:
        published_date = feed_entry.published
        if last_check < published_date:
            print(f"{last_check} < {published_date}")
            entries.append(feed_entry.summary.value)

    with open(f"{temp_dir}/last_check.txt", "w") as last_check_file:
        last_check_file.write(datetime.now().astimezone().isoformat())
    return list(reversed(entries))


def resolve_group_id_from_name(signal_cli_args, target_group_name):
    """Resolve the group id from the given target_group_name.

    Args:
        signal_cli_args (str[]): Signal CLI command without any operation set
        target_group_name ([type]): Name of the group for that the group id should be resolved
    """
    signal_cli_args.append("listGroups")
    group_array = ""
    with subprocess.Popen(signal_cli_args, stdout=subprocess.PIPE) as popen:
        groups = popen.stdout.read()
        groups_utf8 = groups.decode("UTF-8")
        group_array = groups_utf8.split("\n")
        for group in group_array:
            if target_group_name in group:
                group_id = group.split(" ")[1]
                print(f"Successfully resolved group with name {target_group_name} to id {group_id}")
                return group_id
    print(f"Could not resolve {target_group_name} to a group id. Please check if you misspelled the name. The script found the following groups:")
    for group in group_array:
        print(group)
    return None


def receive_messages(signal_cli_args):
    """In order to allow any further operations the Signal API wants us to fetch messages first.

    We do not neet them, so we will just ignore whatever is sent to us.

    Args:
        signal_cli_args (str[]): Signal CLI command without any operation set
    """
    print("atom-signal-mirror was previously configured. Receiving messages to allow further operations.")
    signal_cli_args.append("receive")
    with subprocess.Popen(signal_cli_args, stdout=subprocess.PIPE) as popen:
        popen.stdout.read()

    print("Messages successfully received. Proceeding...")


def create_qr_code(signal_cli_args):
    """Create QR code for signal linking.

    Args:
        signal_cli_args (str[]): Signal CLI command without any operation sets
    """
    print("atom-signal-mirror was started for the first time. Will link a phone number with Signal in the following step...")
    signal_cli_args.append("link")

    with subprocess.Popen(signal_cli_args, stdout=subprocess.PIPE) as popen:
        stdout = popen.stdout.readline()
        print(stdout)
        signal_uri = stdout.decode("UTF-8")
        if "sgnl://" in signal_uri:
            signal_uri_unquoted = unquote(signal_uri).strip()
            print(
                f"Linking to Signal using the following uri: {signal_uri_unquoted}. ",
                "Please scan the QR code in the Signal App on your phone and link the cli.")
            img = pyqrcode.create(signal_uri_unquoted)
            print(img.terminal())
    print("atom-signal-mirror linked the given phone number successfully. Proceeding...")


class MLStripper(HTMLParser):
    """Acknowledgement for HTML stripping goes to Olivier Le Floch: https://stackoverflow.com/a/925630."""

    def __init__(self):  # noqa
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.text = StringIO()

    def handle_data(self, d):  # noqa
        self.text.write(d)

    def _get_data(self):
        return self.text.getvalue()


def strip_tags(html):  # noqa
    s = MLStripper()
    s.feed(html)
    return s._get_data()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=('Send a source atom feed to a signal target group. Right now only works with an already registered phone number at '
                     'Signal and links as an additional device to that account (like signal-desktop)'))
    parser.add_argument('atom_feed_url', type=str, help='Source atom feed URL')
    parser.add_argument('target_group', type=str, help='Target group to send messages')
    parser.add_argument('--dry-run', dest='dry_run', type=bool, default=False, help='If set to True the script will run with the sandbox environment of Signal')
    parser.add_argument('--signal-cli-cfg', type=str, dest='signal_cli_cfg', default='./signal-cli', help='Path to the signal-cli config directory')
    parser.add_argument('--signal-cli-path', type=str, dest='signal_cli_path',
                        default='./signal-cli-binaries/bin/signal-cli', help='Path to the signal-cli executable')
    parser.add_argument('--temp-path', type=str, dest='temp_path', default='./temp', help='Path to the temp directory')
    args = parser.parse_args()

    atom_signal_mirror(args.target_group, args.signal_cli_path, args.signal_cli_cfg, args.temp_path, args.atom_feed_url, args.dry_run)
